package skills;

import arenas.ArenaPVP;
import players.Player;

public final class Heal implements Skill {
    private final Integer amount;

    private final Player owner;

    public Heal(Player owner, Integer amount) {
        this.owner = owner;
        this.amount = amount;
    }

    @Override
    public void perform(ArenaPVP arena) {
        owner.takeHeal(amount);
    }
}
