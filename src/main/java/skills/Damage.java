package skills;

import arenas.ArenaPVP;
import players.Player;

public final class Damage implements Skill {
    private final Integer amount;
    private final Player owner;

    public Damage(Player owner, Integer amount) {
        this.owner = owner;
        this.amount = amount;
    }

    @Override
    public void perform(ArenaPVP arena) {
        if(arena.getFirstPlayer() == owner){
            arena.getSecondPlayer().takeDamage(amount);
        } else {
            arena.getFirstPlayer().takeDamage(amount);
        }
    }
}
