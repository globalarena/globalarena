package ai;

import arenas.ArenaPVP;
import players.Archer;
import players.Mage;
import players.Player;
import skills.Skill;

public class Main {
    public static void main(String[] args) {
        Player archer = new Archer();
        Player mage = new Mage();

        ArenaPVP arena = new ArenaPVP(archer, mage);

        AI ai = new AI();


        ArenaPVP arena2 = new ArenaPVP(arena);

//        while(!(arena.getFirstPlayer().isDead() || arena.getSecondPlayer().isDead())){
//            Skill player1 = ai.getNextSkill(arena.getFirstPlayer(), arena);
//            Skill player2 = ai.getNextSkill(arena.getSecondPlayer(), arena);
//
//            player1.perform(arena);
//            player2.perform(arena);
//        }
//
//        if(arena.getFirstPlayer().isDead()){
//            System.out.println("Player 1 is dead");
//        } else {
//            System.out.println("Player 1 win");
//        }
//
//        if(arena.getSecondPlayer().isDead()){
//            System.out.println("Player 2 is dead");
//        } else {
//            System.out.println("Player 2 win");
//        }
    }
}
