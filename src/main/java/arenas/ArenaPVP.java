package arenas;

import players.Player;

public class ArenaPVP implements Arena{
    private final Player firstPlayer;
    private final Player secondPlayer;
    private int currentPlayer;

    public ArenaPVP(Player firstPlayer, Player secondPlayer){
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    public ArenaPVP(ArenaPVP arenaPVP) {
        this.firstPlayer = arenaPVP.firstPlayer.clone();
        this.secondPlayer = arenaPVP.secondPlayer.clone();
        this.currentPlayer = arenaPVP.currentPlayer;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public int getCurrentPlayer(){return currentPlayer;}

    public void setCurrentPlayer(int currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}
