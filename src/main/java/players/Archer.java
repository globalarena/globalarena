package players;

import players.common.RangeValue;
import skills.Damage;
import skills.Skill;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Archer implements Player {

    private RangeValue health;
    private List<Skill> skills;

    public Archer() {
        health = new RangeValue(150);
        skills = new ArrayList<>();
        skills.add(new Damage(this, 15));
    }

    public Archer(Archer archer){
        this.health = new RangeValue(archer.health);
        this.skills = archer.skills;
    }

    @Override
    public void takeDamage(Integer damageValue) {
        int randValue = new Random().nextInt(100);
        int damageAmount = randValue > 20 ? damageValue : 0;
        health.decrement(damageAmount);
    }

    @Override
    public void takeHeal(Integer amount) {
        health.increment(amount);
    }

    @Override
    public Boolean isDead() {
        return health.isMin();
    }

    @Override
    public List<Skill> getSkills() {
        return skills;
    }

    @Override
    public Player clone() {
        return new Archer(this);
    }
}
