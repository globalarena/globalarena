package players;

import players.common.Characteristic;
import players.common.RangeValue;
import skills.Damage;
import skills.Heal;
import skills.Skill;

import java.util.ArrayList;
import java.util.List;

import static players.common.Characteristic.*;

public class Mage implements Player{
    private List<Skill> skills;
    private RangeValue health;

    public Mage() {
        health = new RangeValue(100);

        skills = new ArrayList<>();
        skills.add(new Damage(this, 15));
        skills.add(new Heal(this, 20));

    }

    @Override
    public void takeDamage(Integer damageValue) {
        health.decrement(damageValue);
    }

    @Override
    public void takeHeal(Integer amount) {
        health.increment(amount);
    }

    @Override
    public Boolean isDead() {
        return health.isMin();
    }

    @Override
    public List<Skill> getSkills() {
        return skills;
    }

    @Override
    public Player clone() {
        return null;
    }
}
