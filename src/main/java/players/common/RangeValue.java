package players.common;

public final class RangeValue {

    private static final Integer DEFAULT_MIN = 0;
    private static final Integer DEFAULT_MAX = 100;

    private Integer value;
    private Integer min;
    private Integer max;

    public RangeValue(Integer value) {
        this(value, DEFAULT_MIN, DEFAULT_MAX);
    }

    public RangeValue(RangeValue rangeValue) {
        this.value = rangeValue.value;
        this.min = rangeValue.min;
        this.max = rangeValue.max;
    }


    public RangeValue(Integer value, Integer min, Integer max) {
        this.value = value;
        this.min = min;
        this.max = max;
    }

    public void increment(Integer amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Value cannot be negative");
        }
        value = Math.min(value + amount, max);
    }

    public void decrement(Integer amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Value cannot be negative");
        }
        value = Math.max(value - amount, min);
    }

    public Integer getMin() {
        return min;
    }

    public Integer get() {
        return value;
    }

    public boolean isMin() {
        return value == min;
    }
}
